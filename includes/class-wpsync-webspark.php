<?

require_once(ABSPATH . '/wp-admin/includes/image.php');

class WpsyncWebspark {

    private $url;
    private $limit;

    function __construct(){
        $this->url = 'https://wp.webspark.dev/wp-api/products';
        $this->limit = 100;
    }

    public function getItems(){
        $response = wp_remote_get($this->url, ['timeout' => 30 , 'method' => 'GET']);
        if (is_wp_error($response)) {
            throw new Exception($response->get_error_message());
            return false;
        }
        $data = json_decode(wp_remote_retrieve_body($response),true);
        if(empty($data) || $data['error'] == true){
            return false;
        }
        
        return $data['data'];
    }

    private function updateItem($product_id, $item){
        $product = array(
            'ID'           => $product_id,
            'post_title' => $item['name'],
            'post_content' => $item['description']
        );
        wp_update_post($product);
        $price = preg_replace('/[^0-9.]/', '', $item['price']);
        update_post_meta($product_id, '_price', $price);
        update_post_meta($product_id, '_stock', $item['in_stock']);

        return true;
    }

    private function insertItem($item) {
        $product = [
            'post_title' => $item['name'],
            'post_content' => $item['description'],
            'post_status' => 'publish',
            'post_type' => 'product',
        ];
        $product_id = wp_insert_post($product);
        $price = preg_replace('/[^0-9.]/', '', $item['price']);
        update_post_meta($product_id, '_price', $price);
        update_post_meta($product_id, '_sku', $item['sku']);
        update_post_meta($product_id, '_stock_status', 'instock');
        update_post_meta($product_id, '_manage_stock', 'yes');
        update_post_meta($product_id, '_stock', $item['in_stock']);
        $this->setThumb($product_id,$item['picture']);

        return true;
    }

    private function setThumb($post_id,$image_url){
        $ch = curl_init($image_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        $url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);
        $upload_dir = wp_upload_dir();
        $image_data = file_get_contents($url);
        $filename = basename($url);
        if(wp_mkdir_p($upload_dir['path']))
          $file = $upload_dir['path'] . '/' . $filename;
        else
          $file = $upload_dir['basedir'] . '/' . $filename;
        file_put_contents($file, $image_data);
        $wp_filetype = wp_check_filetype($filename, null );
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => sanitize_file_name($filename),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
        wp_update_attachment_metadata( $attach_id, $attach_data );
        set_post_thumbnail( $post_id, $attach_id );
    }

    public function execClear() {
        global $wpdb;
        $json = file_get_contents(WSW_PATH.'/sku.json');
        $items = json_decode($json, true);
        if(empty($items)) return false;
        
        $skus = "'" . implode("','", $items) . "'";
        $query = $wpdb->prepare("
            SELECT post_id
            FROM $wpdb->postmeta
            WHERE meta_key = '_sku'
            AND meta_value NOT IN ($skus)
        ");
        $results = $wpdb->get_results($query);
        var_dump($results);
        if ($results) {
            foreach ($results as $item) {
                wp_delete_post($item->post_id, true);
            }
        } 
    }

    public function cronStart(){
        $items = $this->getItems();
        file_put_contents( WSW_PATH.'/items.json', $items);
        $sku = [];
        foreach ($items as $item) {
            $sku[] = $item['sku'];
        }
        $json = json_encode($sku);
        $items = json_encode($items);
        file_put_contents( WSW_PATH.'/sku.json', $json);
        file_put_contents( WSW_PATH.'/items.json', $items);
        wp_schedule_single_event(time(), 'wsw_execQueue');
    }
    
    public function execQueue(){
        $json = file_get_contents(WSW_PATH.'/items.json');
        $items = json_decode($json, true);
        $count = 0;
        if($items == false) return false;
        $sku = [];
        foreach ($items as $item) {
            $product_id = $this->itemExist($item['sku']);
            
            if($product_id){
                $this->updateItem($product_id, $item);
                echo $product_id.' ';
            }else{
                $this->insertItem($item);
            }
            $sku[] = $item['sku'];
            $count++;
            if ($count >= $this->limit) {
                break;
            }
        }
        $queue_data = array_slice($items, $count);
        if(!empty($queue_data)){
            file_put_contents(WSW_PATH.'/items.json', json_encode($queue_data));
            wp_schedule_single_event(time(), 'wsw_execQueue');
        }else{
            file_put_contents(WSW_PATH.'/items.json', json_encode([]));
            wp_schedule_single_event(time(), 'wsw_execClear');
        }
    }

    public function itemExist($sku){
        global $wpdb;
        $query = $wpdb->prepare(
            "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = '_sku' AND meta_value = %s",
            $sku
        );
        $post_id = $wpdb->get_var($query);
        if ($post_id) {
            return $post_id;
        } else {
            return false;
        }
    }

}