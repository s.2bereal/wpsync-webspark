<?
/**
 * Plugin Name:       Wpsync Webspark
 * Plugin URI:        http://localhost/
 * Description:       Wordpress plugin for sync products Woocommerce
 * Version:           1.0
 * Author:            Serg K
 * Text Domain:       wpsync-webspark
 */

define( 'WSW_PLUGIN_NAME', 'wpsync-webspark' );
define( 'WSW_VERSION', '1.0' );
define( 'WSW_PATH', plugin_dir_path( __FILE__ ) );

require_once WSW_PATH . '/includes/class-wpsync-webspark.php';

function activate_wpsync_webspark() {
    if ( ! wp_next_scheduled( 'wsw_cron' ) ) {
         wp_schedule_event( time(), 'hourly', 'wsw_cron' );
    }
}

function deactivate_wpsync_webspark() {
    wp_clear_scheduled_hook( 'wsw_cron_hook' );
}

register_activation_hook( __FILE__, 'activate_wpsync_webspark' );
register_deactivation_hook( __FILE__, 'deactivate_wpsync_webspark' );

function wsw_cron_hook(){
    $wpSync = new WpsyncWebspark();
    $wpSync->cronStart();
}
add_action( 'wsw_cron', 'wsw_cron_hook' );

add_action('wsw_execQueue', function() {
    $wpSync = new WpsyncWebspark();
    $wpSync->execQueue();
});

add_action('wsw_execClear', function() {
    $wpSync = new WpsyncWebspark();
    $wpSync->execClear();
});